package com.devcamp.carapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.carapi.models.CarType;

public interface CarTypeRepository extends JpaRepository<CarType, Integer> {

}
