package com.devcamp.carapi.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.carapi.models.Car;
import com.devcamp.carapi.models.CarType;
import com.devcamp.carapi.services.CarService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CarController {
    @Autowired
    private CarService carService;

    @GetMapping("/create-car")
    public void createCar() {
        carService.createCars();
    }

    @GetMapping("/depcamp-cars")
    public List<Car> getCars() {
        return carService.getCars();
    }

    @GetMapping("/depcamp-carType")
    public Set<CarType> getCarType(@RequestParam("carCode") String carCode) {
        return carService.getCarType(carCode);
    }
}
